﻿using System.Diagnostics;
using System.IO;

namespace PolygonToBacsConverter
{
    public class TestsConverter : IConverter
    {
        public void Convert(string fromPath, string toPath)
        {
            string polygonPath = Path.Combine(fromPath, @"tests\");
            string bacsPath = Path.Combine(toPath, @"tests\");
            IO.CreateDirectory(bacsPath);
            GenerateTests(fromPath);
            IO.CopyDirectory(polygonPath, bacsPath);
            RenameTests(bacsPath);
        }

        private void GenerateTests(string polygonPath)
        {
            var process = new Process();
            string doAllPath = Path.Combine(polygonPath, @"doall.bat");
            process.StartInfo.FileName = doAllPath;
            process.StartInfo.WorkingDirectory = polygonPath;
            process.Start();
            process.WaitForExit();
        }

        private void RenameTests(string testsPath)
        {
            var directory = new DirectoryInfo(testsPath);
            var files = directory.GetFiles();
            foreach (var file in files)
            {
                string path = Path.ChangeExtension(file.FullName, string.IsNullOrEmpty(file.Extension) ? ".in" : ".out");
                file.MoveTo(path);
            }
        }
    }
}