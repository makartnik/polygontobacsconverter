﻿using System.IO;

namespace PolygonToBacsConverter
{
    public class MiscConverter : IConverter
    {
        public void Convert(string fromPath, string toPath)
        {
            string bacsPath = Path.Combine(toPath, @"misc\");
            IO.CreateDirectory(bacsPath);
            CopySolutions(fromPath, toPath);
            CopyStatements(fromPath, toPath);
        }

        private void CopySolutions(string polygonRoot, string bacsRoot)
        {
            string polygonPath = Path.Combine(polygonRoot, @"solutions\");
            string bacsPath = Path.Combine(bacsRoot, @"misc\", @"solutions\");
            IO.CreateDirectory(bacsPath);
            IO.CopyDirectory(polygonPath, bacsPath);
        }

        private void CopyStatements(string polygonRoot, string bacsRoot)
        {
            string polygonPath = Path.Combine(polygonRoot, @"statements\");
            string bacsPath = Path.Combine(bacsRoot, @"misc\", @"statements\");
            IO.CreateDirectory(bacsPath);
            IO.CopyDirectory(polygonPath,bacsPath);
        }

}
}