﻿using System.IO;

namespace PolygonToBacsConverter
{
    public class CheckerConverter : IConverter
    {
        public void Convert(string fromPath, string toPath)
        {
            string bacsPath = Path.Combine(toPath, @"checker\");
            IO.CreateDirectory(bacsPath);
            CopyChecker(fromPath, toPath);
            CreateConfiguration(toPath);
        }

        private void CopyChecker(string polygonRoot, string bacsRoot)
        {
            string polygonPath = Path.Combine(polygonRoot, @"files\", @"check.cpp");
            string bacsPath = Path.Combine(bacsRoot, @"checker\", @"check.cpp");
            File.Copy(polygonPath, bacsPath, true);
        }

        private void CreateConfiguration(string bacsRoot)
        {
            string path = Path.Combine(bacsRoot, @"checker\", @"config.ini");
            using (var writer = new StreamWriter(path))
            {
                writer.Write(Properties.Resources.CheckerConfiguration);
            }
        }
    }
}