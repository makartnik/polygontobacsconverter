﻿using System.IO;

namespace PolygonToBacsConverter
{
    public class RootConverter : IConverter
    {
        public void Convert(string fromPath, string toPath)
        {
            CreateFormat(toPath);
        }

        private void CreateFormat(string bacsPath)
        {
            string filename = Path.Combine(bacsPath, "format");
            string content = "bacs/problem/single#simple0";
            using (var writer = new StreamWriter(filename))
            {
                writer.WriteLine(content);
            }
        }
    }
}