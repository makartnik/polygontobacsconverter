﻿using System.IO;
using System.Linq;

namespace PolygonToBacsConverter
{
    public class StatementConverter : IConverter
    {
        public void Convert(string fromPath, string toPath)
        {
            string bacsPath = Path.Combine(toPath, @"statement\");
            IO.CreateDirectory(bacsPath);
            CopyPdf(fromPath, toPath);
            CreateConfiguration(toPath);
        }


        private void CopyPdf(string polygonRoot, string bacsRoot)
        {
            string bacsPath = Path.Combine(bacsRoot, @"statement\statement.pdf");
            string pdfDirectoryPath = Path.Combine(polygonRoot, @"statements\", @".pdf\");
            var pdfDirectory = new DirectoryInfo(pdfDirectoryPath);
            var pdfFiles = pdfDirectory.GetFiles("*.pdf", SearchOption.AllDirectories);
            if (pdfFiles.Any())
            {
                pdfFiles.First().CopyTo(bacsPath);
            }
        }

        private void CreateConfiguration(string bacsRoot)
        {
            string path = Path.Combine(bacsRoot, @"statement\", @"pdf.ini");
            using (var writer = new StreamWriter(path))
            {
                writer.Write(Properties.Resources.StatementConfiguration);
            }
        }
    }
}