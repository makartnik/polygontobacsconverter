﻿using System;
using System.IO;

namespace PolygonToBacsConverter
{
    public class ZipConverter : IConverter
    {
        private readonly DirectoryConverter _directoryConverter = new DirectoryConverter();

        private string GenerateTempPath()
        {
            string temp = Path.GetTempPath();
            string guid = Guid.NewGuid().ToString();
            return Path.Combine(temp, guid);
        }

        public void Convert(string fromPath, string toPath)
        {
            string tempPath = GenerateTempPath();
            string polygonPath = Path.Combine(tempPath, @"polygon\");
            string bacsPath = Path.Combine(tempPath, @"bacs\");

            try
            {
                IO.CreateDirectory(tempPath);
                IO.ExtractZipArchive(fromPath, polygonPath);
                _directoryConverter.Convert(polygonPath, bacsPath);
                IO.CreateZipArchive(bacsPath, toPath);
            }
            finally
            {
                IO.DeleteDirectory(tempPath);
            }
        }
    }
}
