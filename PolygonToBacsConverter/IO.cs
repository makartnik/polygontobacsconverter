﻿using System.IO;
using System.IO.Compression;

namespace PolygonToBacsConverter
{
    internal static class IO
    {
        public static void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
                Directory.Delete(path, true);
        }

        public static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public static void CreateZipArchive(string sourcePath, string archivePath)
        {
            if (File.Exists(archivePath))
                File.Delete(archivePath);
            ZipFile.CreateFromDirectory(sourcePath, archivePath);
        }

        public static void ExtractZipArchive(string archivePath, string directoryPath)
        {
            ZipFile.ExtractToDirectory(archivePath, directoryPath);
        }

        public static void CopyDirectory(string from, string to)
        {
            var sourceDirectory = new DirectoryInfo(from);

            var files = sourceDirectory.GetFiles();

            foreach (var file in files)
            {
                string path = Path.Combine(to, file.Name);
                file.CopyTo(path);
            }

            var directories = sourceDirectory.GetDirectories();

            foreach (var directory in directories)
            {
                string path = Path.Combine(to, directory.Name);
                CreateDirectory(path);
                CopyDirectory(directory.FullName, path);
            }
        }

    }
}