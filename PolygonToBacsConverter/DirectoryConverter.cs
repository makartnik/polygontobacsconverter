﻿using System.Collections.Generic;

namespace PolygonToBacsConverter
{
    public class DirectoryConverter : IConverter
    {
        private readonly List<IConverter> _converters;

        public DirectoryConverter()
        {
            _converters = new List<IConverter>
            {
                new CheckerConverter(),
                new MiscConverter(),
                new StatementConverter(),
                new TestsConverter(),
                new RootConverter()
            };
        }

        public void Convert(string fromPath, string toPath)
        {
            foreach (var converter in _converters)
                converter.Convert(fromPath, toPath);
        }
    }
}