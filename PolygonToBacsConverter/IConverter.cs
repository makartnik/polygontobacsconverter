﻿namespace PolygonToBacsConverter
{
    public interface IConverter
    {
        void Convert(string fromPath, string toPath);
    }
}