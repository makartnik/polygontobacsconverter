﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PolygonToBacsConverter;

namespace DemoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var converter = new ZipConverter();
            string polygonPath = args[0];
            string bacsPath = args[1];
            converter.Convert(polygonPath, bacsPath);
        }
    }
}
